import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemoSiComponent } from './demo-si.component';

describe('DemoSiComponent', () => {
  let component: DemoSiComponent;
  let fixture: ComponentFixture<DemoSiComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DemoSiComponent]
    });
    fixture = TestBed.createComponent(DemoSiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
