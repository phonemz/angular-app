import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms'; // Import FormsModule


@Component({
    selector: 'app-demo-si',
    templateUrl: './demo-si.component.html',
    styleUrls: ['./demo-si.component.css']
})
export class DemoSiComponent {
    Name = ''
    FSAD = {
        name:'Somesh',
        classroom: 'CS106',
        classimage: '/assets/classroom.png'
    }
    whenChanged(event:any) {
        this.Name = event.target.value
    }
    
}
