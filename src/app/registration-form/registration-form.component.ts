import { Component } from '@angular/core';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent {
  user = {
    username: '',
    email: '',
    password: '',
    confirmPassword: ''
  };

  passwordsDoNotMatch = false;

  onSubmit(form: any) {
    if (form.valid && this.user.password === this.user.confirmPassword) {
      this.passwordsDoNotMatch = false;
      // Form is valid, and passwords match - handle form submission here
      console.log('Registration data:', this.user);
    } else {
      this.passwordsDoNotMatch = true;
    }
  }
}
