import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-template-driven-form-email-validation",
  templateUrl: "./template-driven-form-email-validation.component.html",
  styleUrls: ["./template-driven-form-email-validation.component.css"]
})
export class TemplateDrivenFormEmailValidationComponent implements OnInit {
  user: any = { name: "", email: "" };
  emailValidation: boolean = true;

  constructor() {}

  ngOnInit() {}

  submit(form:any) {
    console.log(form);
  }
}
