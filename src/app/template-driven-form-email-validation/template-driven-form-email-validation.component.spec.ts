import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateDrivenFormEmailValidationComponent } from './template-driven-form-email-validation.component';

describe('TemplateDrivenFormEmailValidationComponent', () => {
  let component: TemplateDrivenFormEmailValidationComponent;
  let fixture: ComponentFixture<TemplateDrivenFormEmailValidationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TemplateDrivenFormEmailValidationComponent]
    });
    fixture = TestBed.createComponent(TemplateDrivenFormEmailValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
