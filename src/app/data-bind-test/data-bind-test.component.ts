import { Component } from '@angular/core';

@Component({
  selector: 'app-data-bind-test',
  templateUrl: './data-bind-test.component.html',
  styleUrls: ['./data-bind-test.component.css']
})
export class DataBindTestComponent {
  title = 'udemy';
  message = 'online learning';
  display :boolean=false;
  onClick() {
    this.display = true;
  }
}
