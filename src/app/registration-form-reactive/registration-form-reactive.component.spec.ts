import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationFormReactiveComponent } from './registration-form-reactive.component';

describe('RegistrationFormReactiveComponent', () => {
  let component: RegistrationFormReactiveComponent;
  let fixture: ComponentFixture<RegistrationFormReactiveComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RegistrationFormReactiveComponent]
    });
    fixture = TestBed.createComponent(RegistrationFormReactiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
