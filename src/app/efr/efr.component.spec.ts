import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EfrComponent } from './efr.component';

describe('EfrComponent', () => {
  let component: EfrComponent;
  let fixture: ComponentFixture<EfrComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EfrComponent]
    });
    fixture = TestBed.createComponent(EfrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
