import { Component } from '@angular/core';
import { HighlightDirective } from '../directive/highlight.directive';

@Component({
  selector: 'app-efr',
  templateUrl: './efr.component.html',
  styleUrls: ['./efr.component.css'],
  template:`
    <button (click)='onClick()'>Click me</button>
    <input (keyup)='onKeyUp($event)'>`
})
export class EfrComponent {
  items: string[] = ['Item 1', 'Item 2', 'Item 3', 'Item 4'];
  condition: string = 'C'

  onClick() {
    console.log('Button clicked')
  }

  onKeyUp(event:any) {
    console.log('key up:', event.target.value)
  }
  display = false
  onClickFSAD() {
    this.display = true
  }
  
  someVariable = ''
  onKeyUpFSAD(event: any) {
    this.someVariable = event.target.value
  }
}
