import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appHoverHighlight]'
})
export class HoverHighlightDirective {

  constructor(private el: ElementRef) {
    el.nativeElement.classList.add('hover-effect');
   }

}
