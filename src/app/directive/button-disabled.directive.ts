import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appButtonDisabled]'
})
export class ButtonDisabledDirective {

  constructor(private el: ElementRef) {
    this.el.nativeElement.addEventListener('click', () => {
      this.el.nativeElement.setAttribute('disabled', 'true');
    })
  }
}
