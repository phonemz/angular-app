import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'; // Import FormsModule

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DemoSiComponent } from './demo-si/demo-si.component';
import { DataBindTestComponent } from './data-bind-test/data-bind-test.component';
import { EfrComponent } from './efr/efr.component';
import { HighlightDirective } from './directive/highlight.directive';
import { HoverHighlightDirective } from './directive/hover-highlight.directive';
import { ButtonDisabledDirective } from './directive/button-disabled.directive';

import { ReactiveFormsModule } from '@angular/forms';
import { TemplateDrivenFormEmailValidationComponent } from './template-driven-form-email-validation/template-driven-form-email-validation.component';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { RegistrationFormReactiveComponent } from './registration-form-reactive/registration-form-reactive.component';


@NgModule({
  declarations: [
    AppComponent,
    DemoSiComponent,
    DataBindTestComponent,
    EfrComponent,
    HighlightDirective,
    HoverHighlightDirective,
    ButtonDisabledDirective,
    TemplateDrivenFormEmailValidationComponent,
    RegistrationFormComponent,
    RegistrationFormReactiveComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
